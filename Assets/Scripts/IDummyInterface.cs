﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Go for specific interfaces instead of general

public interface IDummyInterface
{
    bool IsWearingHat();
}
