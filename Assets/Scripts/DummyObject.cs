﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyObject : MonoBehaviour, IDummyInterface
    //Added the interface to implement the method. CTRL+. to implement.
{
    public bool _isWearingHat;

    public bool IsWearingHat() //<- this is the method signature
    {
        //throw new System.NotImplementedException(); -> can usually get rid of this
        return _isWearingHat;
    }

    private void Awake()
    {
        DummyLocator.value = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
