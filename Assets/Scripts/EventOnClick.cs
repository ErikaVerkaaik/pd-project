﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class EventOnClick : MonoBehaviour, IPointerClickHandler
{
    public Animation PlayOnClick;

    public void OnPointerClick(PointerEventData eventData)
    {
        PlayOnClick.Play();
    }
}
