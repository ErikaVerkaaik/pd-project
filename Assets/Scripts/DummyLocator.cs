﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DummyLocator
{
    //static means that there won't be instances of this!

    static IDummyInterface _value = new DummyNull();
    public static IDummyInterface value // this is a property
    {
        get
        {
            return _value;
        }

        set
        {
            _value = value;

            if (_value == null)
            {
                _value = new DummyNull();
            }


        }
    }

}

public class DummyNull : IDummyInterface
{
    public bool IsWearingHat()
    {
        Debug.LogError("Error for missing Object in scene");
        return false;
    }
}
